
Our company started on Cuba Street in Wellington, New Zealand

# Founded: 2013 #

We opened shop with the vision to serve amateur photographers around the beautiful Wellington region, enabling them to capture that inspiring event at just the right moment.

# Our Goal: Quick snap! #

"The best camera is the one that's with you" – Chase Jarvis

We put affordable, easy to use film and digital cameras in the hands of creative individuals for them to carry around at all times.